﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IndependentWorkNumberOne.Data.Repositories.UnitOfWork;
using Microsoft.AspNetCore.Mvc;

namespace IndependentWorkNumberOne.Controllers
{
    public class BaseController : Controller
    {
        protected readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public BaseController(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }
    }
}