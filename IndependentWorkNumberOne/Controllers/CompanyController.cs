﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using IndependentWorkNumberOne.Data.DataBase;
using IndependentWorkNumberOne.Data.Repositories.UnitOfWork;
using IndependentWorkNumberOne.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IndependentWorkNumberOne.Controllers
{
    public class CompanyController : BaseController
    {
        public CompanyController(IUnitOfWorkFactory unitOfWorkFactory) : base(unitOfWorkFactory)
        {
            //this.unitOfWorkFactory = unitOfWorkFactory;
        }

        private ListModel GetListModel()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var listModel = new ListModel();
                listModel.Title = "Список компаний";
                listModel.Companies = unitOfWork.Companies.GetAll().Select(x => new CompanyModel
                {
                    ID = x.Id,
                    Name = x.Name,
                    CityID = x.CityId,
                    City = (x.City != null) ? new CityModel { Name = x.City.Name} : new CityModel{Name = string.Empty}
                    //CompanyList =
                });
                return listModel;
            }
        }

        // GET: City
        public ActionResult Index()
        {
            return View(GetListModel());
        }

        // GET: City/Create
        public ActionResult Create()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var companyModel = new CompanyModel {Title = "Создать компанию"};
                companyModel.Cities = unitOfWork.Cities.GetAll().Select(x =>
                    new SelectListItem {Text = x.Name, Value = x.Id.ToString()}).ToList();
                return View(companyModel);
            }
        }

        // POST: City/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CompanyModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                unitOfWork.Companies.Add(new Company { Name = model.Name, CityId = model.CityID});
                return View("Index", GetListModel());
            }
        }

        // GET: City/Edit/5
        public ActionResult Edit(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                Company company = unitOfWork.Companies.GetById(id);
                if (company != null)
                {
                    var companyModel = new CompanyModel
                        {ID = id, Name = company.Name, CityID = company.CityId, Title = "Редактирование"};
                    companyModel.Cities = unitOfWork.Cities.GetAll().Select(x =>
                        new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToList();
                    return View(companyModel);
                }
                else
                    return View("Index", GetListModel());
            }
        }

        // POST: City/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CompanyModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                unitOfWork.Companies.Update(new Company { Id = model.ID, Name = model.Name, CityId = model.CityID});
                return View("Index", GetListModel());
            }
        }

        // GET: City/Delete/5
        public ActionResult Delete(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var conpany = unitOfWork.Companies.GetById(id);
                conpany.City = unitOfWork.Cities.GetById(conpany.CityId);
                if (conpany != null)
                    return View(new CompanyModel { ID = id, Name = conpany.Name, CityID = conpany.CityId,
                                                   City = new CityModel{Name = conpany.City.Name}, Title = "Удаление" });
                else
                    return View("Index", GetListModel());
            }
        }

        // POST: City/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(CompanyModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                unitOfWork.Companies.Delete(new Company { Id = model.ID, Name = model.Name, CityId = model.CityID});
                return View("Index", GetListModel());
            }
        }
    }
}