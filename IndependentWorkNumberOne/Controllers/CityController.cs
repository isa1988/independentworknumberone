﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IndependentWorkNumberOne.Data.DataBase;
using IndependentWorkNumberOne.Data.Repositories.UnitOfWork;
using IndependentWorkNumberOne.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace IndependentWorkNumberOne.Controllers
{
    public class CityController : BaseController
    {
        public CityController(IUnitOfWorkFactory unitOfWorkFactory) : base(unitOfWorkFactory)
        {
            //this.unitOfWorkFactory = unitOfWorkFactory;
        }

        private ListModel GetListModel()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var listModel = new ListModel();
                listModel.Title = "Список городов";
                listModel.Cities = unitOfWork.Cities.GetAll().Select(x => new CityModel
                {
                    ID = x.Id,
                    Name = x.Name,
                    //CompanyList =
                });
                return listModel;
            }
        }

        // GET: City
        public ActionResult Index()
        {
            return View(GetListModel());
        }
        
        // GET: City/Create
        public ActionResult Create()
        {
            return View(new CityModel{Title = "Создать модель"});
        }

        // POST: City/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CityModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                unitOfWork.Cities.Add(new City{Name = model.Name});
                return View("Index", GetListModel());
            }
        }

        // GET: City/Edit/5
        public ActionResult Edit(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var city = unitOfWork.Cities.GetById(id);
                if (city != null)
                    return View(new CityModel { ID = id, Name = city.Name, Title =  "Редактирование"});
                else
                    return View("Index", GetListModel());
            }
        }

        // POST: City/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CityModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                unitOfWork.Cities.Update(new City{Id = model.ID, Name = model.Name});
                return View("Index", GetListModel());
            }
        }

        // GET: City/Delete/5
        public ActionResult Delete(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var city = unitOfWork.Cities.GetById(id);
                if (city != null)
                    return View(new CityModel { ID = id, Name = city.Name, Title = "Удаление"});
                else
                    return View("Index", GetListModel());
            }
        }

        // POST: City/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(CityModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                unitOfWork.Cities.Delete(new City { Id = model.ID, Name = model.Name });
                return View("Index", GetListModel());
            }
        }
    }
}