﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace IndependentWorkNumberOne.Models
{
    public class CityModel : PageModel
    {
        public int ID { get; set; }

        [DisplayName("Наименование")]
        public string Name { get; set; }

        public  IEnumerable<CompanyModel> CompanyList { get; set; }

        public CityModel()
        {
            CompanyList = new List<CompanyModel>();
        }
    }
}
