﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndependentWorkNumberOne.Models
{
    public class ListModel : PageModel
    {
        public IEnumerable<CityModel> Cities { get; set; }
        public IEnumerable<CompanyModel> Companies { get; set; }

        public ListModel()
        {
            Cities = new List<CityModel>();
            Companies = new List<CompanyModel>();
        }
    }
}
