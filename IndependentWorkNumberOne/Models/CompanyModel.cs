﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace IndependentWorkNumberOne.Models
{
    public class CompanyModel : PageModel
    {
        public int ID { get; set; }

        [DisplayName("Наименование")]
        public string Name { get; set; }

        [DisplayName("Город")]
        public int CityID { get; set; }

        [DisplayName("Город")]
        public CityModel City { get; set; }

        public List<SelectListItem> Cities { get; set; }

        public CompanyModel()
        {
            Cities = new List<SelectListItem>();
        }
    }
}
