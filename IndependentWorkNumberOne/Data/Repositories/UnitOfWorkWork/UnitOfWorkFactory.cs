﻿using IndependentWorkNumberOne.Data.Repositories.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndependentWorkNumberOne.Data.Repositories.UnitOfWorkWork
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private readonly IDataDbContextFactory _dataDbContextFactory;

        public UnitOfWorkFactory(IDataDbContextFactory dataDbContextFactory)
        {
            _dataDbContextFactory = dataDbContextFactory;
        }

        public IUnitOfWork MakeUnitOfWork()
        {
            return new UnitOfWork(_dataDbContextFactory.Create());
        }
    }

}
