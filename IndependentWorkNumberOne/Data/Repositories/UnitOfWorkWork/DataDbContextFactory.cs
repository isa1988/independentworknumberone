﻿using IndependentWorkNumberOne.Data.DataBase;
using IndependentWorkNumberOne.Data.Repositories.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndependentWorkNumberOne.Data.Repositories.UnitOfWorkWork
{
    public class DataDbContextFactory : IDataDbContextFactory
    {
        private readonly DbContextOptions<DataDbContext> _options;

        public DataDbContextFactory(
            DbContextOptions<DataDbContext> options)
        {
            _options = options;
        }

        public DataDbContext Create()
        {
            return new DataDbContext(_options);
        }

    }
}
