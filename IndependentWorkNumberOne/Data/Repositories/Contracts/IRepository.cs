﻿
using IndependentWorkNumberOne.Data.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndependentWorkNumberOne.Data.Repositories.Contracts
{
    public interface IRepository<T> where T : Entity
    {
        int Add(T entity);
        IEnumerable<T> GetAll();
        T GetById(int id);
        void Update(T entity);
        void Delete(T entity);
    }
}
