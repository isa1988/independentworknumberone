﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IndependentWorkNumberOne.Data.DataBase;

namespace IndependentWorkNumberOne.Data.Repositories.Contracts
{
    public interface ICompanyRepository : IRepository<Company>
    {
    }
}
