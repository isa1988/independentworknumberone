﻿using IndependentWorkNumberOne.Data.DataBase;
using IndependentWorkNumberOne.Data.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndependentWorkNumberOne.Data.Repositories.Work
{
    public class CityRepository : Repository<City>, ICityRepository
    {
        public CityRepository(DataDbContext context) : base(context)
        {
            DbSet = context.Cities;
        }

        public override void Check(City company)
        {
            if (company.Name == null || company.Name == string.Empty)
                throw new ArgumentException("Не заполнено наименование");
            if (_context.Cities.Any(x => x.Name == company.Name))
                throw new ArgumentException("В текущем городе уже есть такая компания");
        }
    }
}
