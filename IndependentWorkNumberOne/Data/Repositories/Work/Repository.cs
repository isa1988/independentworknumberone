﻿using IndependentWorkNumberOne.Data.DataBase;
using IndependentWorkNumberOne.Data.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace IndependentWorkNumberOne.Data.Repositories.Work
{
    public class Repository<T> : IRepository<T> where T : Entity
    {
        protected DataDbContext _context;
        protected DbSet<T> DbSet { get; set; }

        public virtual void Check(T entity) {}

        public Repository(DataDbContext context)
        {
            _context = context;
        }

        public int Add(T entity)
        {
            Check(entity);
            DbSet.Add(entity);
            _context.SaveChanges();
            return entity.Id;
        }

        public IEnumerable<T> GetAll()
        {
            return DbSet.ToList();
        }
        //identity
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await DbSet.ToListAsync();
        }

        public T GetById(int id)
        {
            return DbSet.FirstOrDefault(e => e.Id == id);
        }

        public void Update(T entity)
        {
            Check(entity);
            _context.Update(entity);
            _context.SaveChanges();
        }

        public void Delete(T entity)
        {
            DbSet.Remove(entity);
            _context.SaveChanges();
        }
    }

}
