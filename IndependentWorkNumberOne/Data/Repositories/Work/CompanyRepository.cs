﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IndependentWorkNumberOne.Data.DataBase;
using IndependentWorkNumberOne.Data.Repositories.Contracts;

namespace IndependentWorkNumberOne.Data.Repositories.Work
{
    public class CompanyRepository : Repository<Company>, ICompanyRepository
    {
        public CompanyRepository(DataDbContext context) : base(context)
        {
            DbSet = context.Companies;
        }

        public override void Check(Company company)
        {
            if (company.Name == null || company.Name == string.Empty)
                throw new ArgumentException("Не заполнено наименование");
            if (company.CityId == 0)
                throw new ArgumentException("Не заполнен город");
            if (_context.Companies.Any(x => x.Name == company.Name && x.CityId == company.CityId))
                throw new ArgumentException("В текущем городе уже есть такая компания");
        }
    }
}
