﻿using IndependentWorkNumberOne.Data.DataBase;
using IndependentWorkNumberOne.Data.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndependentWorkNumberOne.Data.Repositories.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        ICityRepository Cities { get; }
        ICompanyRepository Companies { get; }

        Task<int> CompleteAsync();
        void BeginTransaction();
        //void BeginTransaction(IsolationLevel level);
        void RollbackTransaction();
        void CommitTransaction();

        IRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity;
    }

}
