﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndependentWorkNumberOne.Data.DataBase
{
    /// <summary>
    /// Город
    /// </summary>
    public class City : Entity
    {
        public string Name { get; set; }

        public ICollection<Company> CompanyListLazy { get; set; }

        public City()
        {
            CompanyListLazy = new List<Company>();
        }

    }

    public class CityConfiguration : IEntityTypeConfiguration<City>
    {
        public void Configure(EntityTypeBuilder<City> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Name).IsRequired().HasMaxLength(100);
        }
    }
}
