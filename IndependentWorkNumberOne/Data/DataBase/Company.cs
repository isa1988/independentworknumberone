﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndependentWorkNumberOne.Data.DataBase
{
    /// <summary>
    /// Компании
    /// </summary>
    public class Company : Entity
    {
        public string Name { get; set; }

        public int CityId { get; set; }

        public City City { get; set; }
    }


    public class CompanyConfiguration : IEntityTypeConfiguration<Company>
    {
        public void Configure(EntityTypeBuilder<Company> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Name).IsRequired().HasMaxLength(100);
            builder.HasOne(p => p.City)
                .WithMany(t => t.CompanyListLazy)
                .HasForeignKey(p => p.CityId);
        }
    }
}
